/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dafin.springboot.domain;

import dafin.springboot.service.DafinDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author amanda
 */
@RestController
@RequestMapping("/api/dafin")
public class DafinController {
    @Autowired
    private DafinDao dafinDao;    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Dafin finDafinById(@PathVariable String id){
        return dafinDao.findOne(id);    }    
    @RequestMapping(method = RequestMethod.POST)
    public void save(@RequestBody Dafin p){
        dafinDao.save(p);
    }    
    @RequestMapping(method = RequestMethod.GET)
    public Page<Dafin> findAll(Pageable pageable){
        return dafinDao.findAll(pageable);
    }    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id){
        Dafin p = dafinDao.findOne(id);
        if (p != null){
            dafinDao.delete(p);
        }
    }    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void edit(@PathVariable String id, @RequestBody Dafin p){
         Dafin dafin = dafinDao.findOne(id);
         if (dafin != null) {
             p.setId(id);
             dafinDao.save(p);
            }           
        }
    }
    
