/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dafin.springboot.service;

import dafin.springboot.domain.User;
import java.io.Serializable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author amanda
 */
public interface UserDao extends PagingAndSortingRepository<User, String>{

    public User findByUsername(String username);
    
}
