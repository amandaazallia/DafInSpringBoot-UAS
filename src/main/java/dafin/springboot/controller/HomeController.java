/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dafin.springboot.controller;

import dafin.springboot.domain.Dafin;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author amanda
 */
@RestController

public class HomeController {
    
    @RequestMapping("/hello")
    public String hello(){
        return "Hello World !";
    }
    
    @RequestMapping(value = "/buku",method = RequestMethod.GET)
    Dafin getBuku() {
        Dafin p = new Dafin();
        p.setNama("Meja");
        p.setJumlah("40");
        
        return p;
    }
    
    @RequestMapping(value = "/ListDafin",method = RequestMethod.GET)
    List<Dafin> getListBuku() {
        
        List<Dafin> dafins = new ArrayList<Dafin>();
        Dafin p = new Dafin();
        p.setNama("Meja");
        p.setJumlah("40l");
        
        Dafin p1 = new Dafin();
        p1.setNama("Bangku");
        p1.setJumlah("40");
        
        dafins.add(p);
        dafins.add(p1);
        
        return dafins;
    }
}
