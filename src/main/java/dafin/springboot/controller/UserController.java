/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dafin.springboot.controller;

import dafin.springboot.domain.User;
import dafin.springboot.service.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author amanda
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/loggedin", method = RequestMethod.GET)
    public User getUserLoggedIn() throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            throw new Exception("Auth Request");
        }
        Object principal = auth.getPrincipal();
        if (principal == null) {
            throw new Exception("Invalid Principal");
        }
        if (!org.springframework.security.core.userdetails.User.class.isAssignableFrom(principal.getClass())) {
            throw new Exception("Invalid Auth Object" + principal.getClass().getName());
        }
        org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User) principal;
        try {
            User u1 = userDao.findByUsername(u.getUsername());
            return u1;  
        } catch (Exception e) {
            throw new Exception(e.getMessage(),e);
        }        
    }

}