angular.module('aplikasiSpringboot')
        .factory('homeService', function ($http){
            return {
                    getDafin: function (){
                            return $http.get("/dafin");
                        },
                    getListDafin: function (){
                            return $http.get("/listDafin");
                    },
                    getDafinFromDb:function (){
                        return $http.get("/api/dafin");
                    },
                    save: function (data){
                        return $http.post("/api/dafin",data);
                    },
                    delete:function(id){
                        return $http.delete("/api/dafin/"+id);
                    },
                    update:function(id,data){
                        return $http.put("/api/dafin/"+id,data);
                    },
                    getLoggedIn:function(){
                        return $http.get("/api/user/loggedin");
                    }
            };
});


