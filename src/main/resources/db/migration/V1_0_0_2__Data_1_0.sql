INSERT INTO dafin(id, nama, jumlah) VALUES 
('1','Meja','40');

INSERT INTO `c_security_role` (`id`, `name`, `description`) VALUES 
('admin', 'admin', 'Application Admin'),
('user', 'user', 'Application User');

INSERT INTO `c_security_permission` (`id`,`permission_label`,`permission_value`) VALUES
('dafin_update','Edit Dafin','ROLE_DAFIN_UPDATE'),
('dafin_view','View Dafin','ROLE_DAFIN_VIEW'),
('dafin_create','Create Dafin','ROLE_DAFIN_CREATE'),
('dafin_delete','Delete Dafin','ROLE_DAFIN_DELETE'),
('user_view','View User','ROLE_USER_VIEW');

INSERT INTO `c_security_role_permission` (`id_role`, `id_permission`) VALUES
('admin','dafin_update'),
('admin','dafin_view'),
('admin','dafin_create'),
('admin','dafin_delete'),
('user','dafin_view'),
('user','user_view'),
('admin','user_view');

INSERT INTO `c_security_user` (`id`,`username`,`password`,`active`,`id_role`) VALUES
('1','dafadmin','admin',true,'admin'),
('2','dafuser','user',true,'user');
